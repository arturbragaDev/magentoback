<?php
/**
 * Created by PhpStorm.
 * User: OKULOS
 * Date: 05/09/2018
 * Time: 10:46
 */


$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.skyhub.com.br/products",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\"product\":{\"sku\":\"1\",\"name\":\"teste\",\"description\":\"Teste Desc\",\"status\":\"enabled\",\"qty\":500,\"price\":500,\"promotional_price\":400,\"cost\":200,\"weight\":10,\"height\":0,\"width\":0,\"length\":0,\"brand\":\"teste\",\"ean\":\"123\",\"nbm\":\"133\",\"categories\":[{\"code\":\"Categoria_teste\",\"name\":\"Teste_Catego\"}],\"images\":[\"foo\",\"foo\",\"foo\"],\"specifications\":[{\"key\":\"foo\",\"value\":\"foo\"}],\"variations\":[{\"sku\":\"foo\",\"qty\":0,\"ean\":\"foo\",\"images\":[\"foo\",\"foo\",\"foo\"],\"specifications\":[{\"key\":\"foo\",\"value\":\"foo\"}]}],\"variation_attributes\":[\"foo\",\"foo\",\"foo\"]}}",
    CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "content-type: application/json",
        "x-accountmanager-key: xk21bPa9jQ",
        "x-api-key: GQFumJDXH3CUe6ZvaBwU",
        "x-user-email: artur@okulos.com.br"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}
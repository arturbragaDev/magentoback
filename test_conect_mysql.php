<?php
/**
 * Created by PhpStorm.
 * User: OKULOS
 * Date: 06/03/2018
 * Time: 20:12
 */



    // Instancia a classe mysqli passando os dados de conexão
    $db = new mysqli('localhost', 'root', '', 'test');

    // Define qual é a codificação de caracteres utilizada pela base de dados
    $db->set_charset('utf8');

    // Verifica se houve algum erro durante a conexão
    if($db->connect_errno > 0) {
        die('Não foi possível realizar a conexão [' . $db->connect_error . ']');
    }

    // Realiza a primeira consulta ao banco de dados
    $sql = 'SELECT  nome, email FROM funcionario';

    if( ! $resultado = $db->query($sql)) {
        die('Erro ao executar a query [' . $db->error . ']');
    }

?>

<table border="1">
    <tr>
        <td>Id</td>
        <td>Nome</td>
        <td>E-mail</td>
    </tr>
    <?php while($campo = $resultado->fetch_assoc()): ?>
        <tr>
            <!-- <td><?=$campo['id'];?></td> -->
            <td><?=$campo['nome'];?></td>
            <td><?=$campo['email'];?></td>
        </tr>
    <?php endwhile ?>
</table>
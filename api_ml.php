<?php
/**
 * Created by PhpStorm.
 * User: OKULOS
 * Date: 08/02/2018
 * Time: 13:09
 */

// ************* Call API: teste
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.mercadolibre.com/items/"  );
curl_setopt($ch, CURLOPT_POST, 1);// set post data to true
curl_setopt($ch, CURLOPT_POSTFIELDS,"username=myname&password=mypass");   // post data
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$json = curl_exec($ch);
curl_close ($ch);

// returned json string will look like this: {"code":1,"data":"OK"}
// "code" may contain an error code and "data" may contain error string instead of "OK"
$obj = json_decode($json);

if ($obj->{'code'} == '1')
{
    $processed = TRUE;
}else {
    $ERROR_MESSAGE = $obj->{'data'};

}

if (!$processed && $ERROR_MESSAGE != '') {
    echo $ERROR_MESSAGE;
}

?>
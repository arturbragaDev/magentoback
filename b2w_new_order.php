<?php
/**
 * Created by PhpStorm.
 * User: OKULOS
 * Date: 05/09/2018
 * Time: 11:27
 */

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.skyhub.com.br/orders/",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\"order\":{\"channel\":\"Submarino\",\"remote_code\":158013873,\"items\":[{\"id\":\"Prod-006-A\",\"qty\":1,\"original_price\":55.22,\"special_price\":0.0}],\"customer\":{\"name\":\"Bruno santos\",\"email\":\"artur@skyhub.com.br\",\"date_of_birth\":\"1998-01-25\",\"gender\":\"male\",\"vat_number\":\"78732371683\",\"phones\":[\"21 3722-3902\",\"21 3722-3902\",\"21 3722-3902\"]},\"billing_address\":{\"street\":\"Rua Fidencio Ramos\",\"number\":\"302\",\"detail\":\"foo\",\"neighborhood\":\"Centro\",\"city\":\"Rio de Janeiro\",\"region\":\"RJ\",\"country\":\"BR\",\"postcode\":\"04551101\"},\"shipping_address\":{\"street\":\"Rua Sacadura Cabral\",\"number\":\"130\",\"detail\":\"foo\",\"neighborhood\":\"Centro\",\"city\":\"Rio de Janeiro\",\"region\":\"RJ\",\"country\":\"BR\",\"postcode\":\"20081262\"},\"payments\":[{\"method\":\"skyhub_payment\",\"description\":\"Skyhub\",\"parcels\":1,\"value\":0.0}],\"shipping_method\":\"Correios PAC\",\"estimated_delivery\":\"2018-02-11\",\"shipping_cost\":0.0,\"interest\":0.0,\"discount\":0.0}}",
    CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "content-type: foo",
        "x-accountmanager-key: xk21bPa9jQ",
        "x-api-key: GQFumJDXH3CUe6ZvaBwU",
        "x-user-email: artur@okulos.com.br"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}
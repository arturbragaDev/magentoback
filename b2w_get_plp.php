<?php
/**
 * Created by PhpStorm.
 * User: OKULOS
 * Date: 06/09/2018
 * Time: 11:35
 */

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.skyhub.com.br/shipments/b2w/",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "content-type: application/json",
        "x-accountmanager-key: xk21bPa9jQ",
        "x-api-key: L_hJjpJsZ2h62V1uhRzg",
        "x-user-email: artur@okulos.com.br"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}